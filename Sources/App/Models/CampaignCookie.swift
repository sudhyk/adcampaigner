import Vapor
import Fluent
import Foundation
import Cookies
import HTTP

struct CampaignCookies: Model {
    var exists: Bool = false
    var id: Node?

    let adcampaignid: String
    let adcampaignname: String
    let application: String
    let customschemeurl: String
    let appstoreurl: String

    init(adcampaignid: String, adcampaignname: String, application: String, customschemeurl: String, appstoreurl: String) {
        self.adcampaignid = adcampaignid
        self.adcampaignname = adcampaignname
        self.application = application
        self.customschemeurl = customschemeurl
        self.appstoreurl = appstoreurl
    }

    init?(request: Request) {
        guard let adcampaignid = request.query?["adcampaignid"]?.string,
            let adcampaignname = request.query?["adcampaignname"]?.string,
            let application = request.query?["application"]?.string,
            let customschemeurl = request.query?["customschemeurl"]?.string,
            let appstoreurl = request.query?["appstoreurl"]?.string,
            application.lowercased() == "AdCampaigner".lowercased() else {
                return nil
        }
        self = CampaignCookies(adcampaignid: adcampaignid, adcampaignname: adcampaignname, application: application, customschemeurl: customschemeurl, appstoreurl: appstoreurl)
    }

    init?(cookies: Cookies) {
        guard let adcampaignid = cookies["adcampaignid"]?.string,
            let adcampaignname = cookies["adcampaignname"]?.string,
            let application = cookies["application"]?.string,
            let customschemeurl = cookies["customschemeurl"]?.string,
            let appstoreurl = cookies["appstoreurl"]?.string,
            application.lowercased() == "AdCampaigner".lowercased() else {
                return nil
        }
        print(adcampaignid)
        print(adcampaignname)
        print(application)
        print(customschemeurl)
        print(appstoreurl)
        self = CampaignCookies(adcampaignid: adcampaignid, adcampaignname: adcampaignname, application: application, customschemeurl: customschemeurl, appstoreurl: appstoreurl)
    }

    // NodeInitializable
    init(node: Node, in context: Context) throws {
        id = try node.extract("id")
        adcampaignid = try node.extract("adcampaignid")
        adcampaignname = try node.extract("adcampaignname")
        application = try node.extract("application")
        customschemeurl = try node.extract("customschemeurl")
        appstoreurl = try node.extract("appstoreurl")
    }

    func makeNode(context: Context) throws -> Node {
        return try Node(node: ["adcampaignid": adcampaignid,
                               "adcampaignname": adcampaignname,
                               "application": application,
                               "customschemeurl": customschemeurl,
                               "appstoreurl": appstoreurl
            ])
    }

    func toDictionary() -> [AnyHashable: Any] {
        return ["adcampaignid": adcampaignid,
                "adcampaignname": adcampaignname,
                "application": application,
                "customschemeurl": customschemeurl,
                "appstoreurl": appstoreurl]
    }

    /**
     The prepare method should call any methods
     it needs on the database to prepare.
     */
    public static func prepare(_ database: Database) throws {
        // Do Nothing
    }

    static func revert(_ database: Database) throws {
        // Do NOthing
    }
}
