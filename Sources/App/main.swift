import Vapor

let drop = Droplet()

let host = "192.168.1.153"


// Universal Linking

drop.get("/") { request in
    return try drop.view.make("home.html")
}

drop.get("home") { request in
        return try drop.view.make("home.html")
}

drop.get("carbohydrates") { request in
    return try drop.view.make("carbohydrates.html")
}
drop.get("protein") { request in
    return try drop.view.make("protein.html")
}
drop.get("fat") { request in
    return try drop.view.make("fat.html")
}
drop.get("vitamins") { request in
    return try drop.view.make("vitamins.html")
}
drop.get("minerals") { request in
    return try drop.view.make("minerals.html")
}
drop.get("water") { request in
    return try drop.view.make("water.html")
}

drop.get("otadownload") { request in
    return try drop.view.make("otadownload.html")
}

drop.get("strategylaunch") { request in
    return try drop.view.make("strategylaunch.html")
}


// Step 1: Try Launching the App
drop.get("launchapp") { request in
    guard let cookiesObject = CampaignCookies(request: request) else {
        throw Abort.custom(status: .failedDependency, message: "Parameters missing")
    }

    return try drop.view.make("launch", cookiesObject.makeNode())
}


// Step 2: App does not exists on the device, Step 1  redirects to Step 2

// Cookies will be set
// adcampaignid = abcd12345
// adcampaignname = crosssell
// application = AdCampaigner
// customschemeurl = capitalone://XXXXX
// appstoreurl = appstore link
drop.group(TrackCampaignCookieMiddleWare()) { (group) in
    group.get("trackandredirect") { request in
        guard let cookiesObject = CampaignCookies(request: request) else {
            throw Abort.custom(status: .failedDependency, message: "Parameters missing")
        }
        return try drop.view.make("openstore", cookiesObject.makeNode())
    }
}

// Step 3: After app install, app will try to verify 
// Cookies set in Step 2 will be passed to this endpoint automatically
// We validate and return appropriately

drop.get("verifycampaign") { request in

    if let cookiesObject = CampaignCookies(cookies: request.cookies) {
        return try drop.view.make("launch", cookiesObject.makeNode())
    }

    return "No Cookies"
}

drop.run()
