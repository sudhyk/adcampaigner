//
//  TrackCampaignCookieMiddleWare.swift
//  AdCampaigner
//
//  Created by Kaanugovi, Sudheendra Kumar on 1/16/17.
//
//

import Vapor
import HTTP
import Foundation
import Cookies

final class TrackCampaignCookieMiddleWare: Middleware {

    func respond(to request: Request, chainingTo next: Responder) throws -> Response {
        // Get all our Campaign Tracking Parameters

        guard let cookiesObject = CampaignCookies(request: request) else {
            throw Abort.custom(status: .failedDependency, message: "Parameters missing")
        }

        // Forward the request to respond and load view
        let response = try next.respond(to: request)

        print("Setting Cookies")
        for (key, value) in cookiesObject.toDictionary() {
            if let key = key as? String, let value = value as? String {
                let cookie = Cookie(name: key, value: value, expires: Date.distantFuture, maxAge: (365 * 24 * 60 * 60 * 1000), domain: host, path: "/", secure: false, httpOnly: true)
                response.cookies.insert(cookie)
                print("\(key) : \(value)")
            }
        }

        return response
    }
}
